import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// React 17 - import ReactDOM from 'react-dom';
ReactDOM.render(<h2>Test<App /></h2>, document.getElementById('root'))

// setTimeout(() => {
//   ReactDOM.render(<h2>Change</h2>, document.getElementById('root'))
// }, 5000)

// React 18
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

// setTimeout(() => {
//   root.render(<h1>changed after destruction</h1>)
// }, 5000)

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
