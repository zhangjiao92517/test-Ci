import './App.css';
import { memo, useCallback, useEffect, useMemo, useRef, useState } from 'react';


const Child = memo((props) => { 
  console.log("memo child")
  return <div>child component
    <button onClick={props.clickChildButton}>Child Button</button>
  </div>
})
function App() {
  const [num, setNum] = useState(1)
  useEffect(() => { 
    console.log("useEffect")
  }, [num])
  const inputRef = useRef(null)
  const clickChildButton = useMemo(() => { return () => setNum(num=>num+1) }, [])
  console.log(clickChildButton,'hi')
  // const clickChildButton = useCallback(() => setNum(num=>num+1), [])
  // test 8
  return (
    <div className="App">
      hello world
      {num}
      {/* <button onClick={() => setNum(num + 1)}>change </button> */}
      <input type="text" value={num} ref={inputRef} onChange={(e) => setNum(e.target.value)} />
      <Child clickChildButton={clickChildButton}/>
    </div>
  );
}

export default App;
